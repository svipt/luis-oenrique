$(document).ready(function() {
    $('.slickContainer').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
    });
    $('.contantFirst').hover (function(){
        $('.firstSection').toggleClass('background');
    });

    var head = $('.header');
    if(head.offset().top >= 100) {
        $('.header').addClass('sticky');
        $('.text-slide').addClass('stickyLink');
        $('.logo').addClass('stickyLogo');
    }
    $(document).scroll(function(){
        if($(this).scrollTop() >= 100) {
            $('.header').addClass('sticky');
            $('.text-slide').addClass('stickyLink');
            $('.logo').addClass('stickyLogo');

        } else {
            $('.header').removeClass('sticky');
            $('.text-slide').removeClass('stickyLink');
            $('.logo').removeClass('stickyLogo');
        }
    });

    // ____________________chart.js
    var data = {
        labels: ["", "", "", "", "", ""],
        datasets: [
            {
                label: "My First dataset",
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "#ddd9d9",
                pointColor: "#c3bfbf",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [52, 83, 72, 96, 82, 57]
            },
            // {
            //     label: "My Second dataset",
            //     fillColor: "rgba(151,187,205,0.2)",
            //     strokeColor: "rgba(151,187,205,1)",
            //     pointColor: "rgba(151,187,205,1)",
            //     pointStrokeColor: "#fff",
            //     pointHighlightFill: "#fff",
            //     pointHighlightStroke: "rgba(151,187,205,1)",
            //     data: [28, 48, 40, 19, 86, 27, 90]
            // }
        ]
    };

    var options = {
        responsive: true,
        maintainAspectRatio: true,
        scaleOverride: true,
        scaleSteps: 5,
        scaleStepWidth: 10,
        scaleStartValue: 50,
        animation: true
    }

    var ctx = $("#chartastic").get(0).getContext("2d");
    var myNewChart = new Chart(ctx).Line(data, options);

    $(".toggle").on("click", function(){
        $(this).toggleClass("is-off")
        var newData = jQuery.extend(true, {}, data);
        myNewChart.destroy();
        $(".toggle").each(function(){
            var toggle = $(this)
            if(toggle.hasClass("is-off")){
                var set = toggle.data("set");
                newData.datasets[set-1] = {};
            }
        });
        var toggledChart = new Chart(ctx).Line(newData, options);
        myNewChart = toggledChart
    })
    // ____________________chart.js end

    $( ".menu-icon" ).click(function() {
        $( this ).toggleClass( "effect1" );
        $('.wrapMenu').slideToggle();
    });

    $(function(){
        $('a[data-target^="anchor"]').bind('click', function(e){
            if($(document).width() < 1200) {
                $('.wrapMenu').slideUp(500);
                // $('.header').toggleClass('stickys');
            };

            var target = $(this).attr('href'),
                offset = $('.header').height() - (-40),
                bl_top = $(target).offset().top - offset-30;

            bl_top_correct = $(target).offset().top - offset - 60;
            bl_top_correct2 = $(target).offset().top - offset - 30;
            $('body, html').animate({scrollTop: bl_top}, 800);
            $('body, html').animate({scrollTop: bl_top_correct}, 200);
            $('body, html').animate({scrollTop: bl_top_correct2}, 300);
            e.preventDefault();
            return false;
        });
    });

    $(function(){
        $('a[data-target^="scrollDown"]').bind('click', function(e){

            var target = $(this).attr('href'),
                offset = $('.header').height() - 32,
                bl_top = $(target).offset().top - offset-30;

            bl_top_correct = $(target).offset().top - offset - 60;
            bl_top_correct2 = $(target).offset().top - offset - 30;
            $('body, html').animate({scrollTop: bl_top}, 800);
            $('body, html').animate({scrollTop: bl_top_correct}, 200);
            $('body, html').animate({scrollTop: bl_top_correct2}, 300);
            e.preventDefault();
            return false;
        });
    });

    $('.slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 1200,
        autoplay: true,
        dots: true,
        arrow: true
    });


});

